package com.parkit.parkingsystem;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.dao.ParkingSpotDAO;
import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import com.parkit.parkingsystem.integration.service.DataBasePrepareService;
import com.parkit.parkingsystem.model.ParkingSpot;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ParkingSpotDAOTest {

    private static DataBaseTestConfig dataBaseTestConfig = new DataBaseTestConfig();
    private static DataBasePrepareService dataBasePrepareService;
    private static ParkingSpotDAO parkingSpotDAO;
    private ParkingSpot parkingSpot;

    @BeforeAll
    private static void setUp() {
        parkingSpotDAO = new ParkingSpotDAO();
        parkingSpotDAO.dataBaseConfig = dataBaseTestConfig;

        dataBasePrepareService = new DataBasePrepareService();
    }

    @BeforeEach
    private void setUpPerTest() {
        dataBasePrepareService.clearDataBaseEntries();
    }
    
    @AfterAll
    private static void dataBaseCleanUp() {
        dataBasePrepareService.clearDataBaseEntries();
    }



    @Test
    public void getNextAvailableSlotTest() {
        int nextCarSpot = parkingSpotDAO.getNextAvailableSlot(ParkingType.CAR);
        int nextBikeSpot = parkingSpotDAO.getNextAvailableSlot(ParkingType.BIKE);

        assertEquals(true, nextCarSpot >= 1 && nextBikeSpot >= 4);
    }

    @Test
    public void updateParkingTest() {
        parkingSpot = new ParkingSpot(2, ParkingType.CAR, false);
        
        boolean oneParkingSpotIsUpdated = parkingSpotDAO.updateParking(parkingSpot);

        assertEquals(true, oneParkingSpotIsUpdated);
    }
}
