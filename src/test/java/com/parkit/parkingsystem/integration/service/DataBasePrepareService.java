package com.parkit.parkingsystem.integration.service;

import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class DataBasePrepareService {

    DataBaseTestConfig dataBaseTestConfig = new DataBaseTestConfig();

    public void clearDataBaseEntries(){
        PreparedStatement ps1 = null, ps2 = null, ps3 = null, ps4 = null;
        
        Connection con = null;
        try{
            con = dataBaseTestConfig.getConnection();

            //set parking entries to available
            ps1 = con.prepareStatement("update parking set available = true");
            ps1.execute();

            //clear ticket entries;
            ps2 = con.prepareStatement("truncate table ticket");
            ps2.execute();
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            dataBaseTestConfig.closePreparedStatement(ps1);
            dataBaseTestConfig.closePreparedStatement(ps2);
            dataBaseTestConfig.closePreparedStatement(ps3);
            dataBaseTestConfig.closePreparedStatement(ps4);
            dataBaseTestConfig.closeConnection(con);
        }
    }


}
