package com.parkit.parkingsystem;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Objects;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.constants.DBConstants;

import org.junit.jupiter.api.Test;


public class DataBaseConfigTest {
    
    public static final DataBaseConfig dataBaseConfig = new DataBaseConfig();

    @Test
    public void getConnectionTest() {
        Connection con = null;
        try {
            con = dataBaseConfig.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error getting connection to DB in getConnection test.");
        }

        assertEquals(true, Objects.nonNull(con));
    }

    @Test
    public void closeConnectionTest() {
        boolean result;
        Connection con = null;
        try {
            con = dataBaseConfig.getConnection();
            dataBaseConfig.closeConnection(con);

            result = con.isClosed();

            assertEquals(true, result);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error closing connection to DB in test.");
        }
    }

    @Test
    public void closePreparedStatementTest(){
        boolean result;
        PreparedStatement ps = null;

        Connection con = null;
        try {
            con = dataBaseConfig.getConnection();

            ps = con.prepareStatement(DBConstants.GET_TICKET);
            ps.setString(1,"ABCDEF");

            dataBaseConfig.closePreparedStatement(ps);

            result = ps.isClosed();

            assertEquals(true, result);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error closing prepared statement in test.");
        } finally {
            dataBaseConfig.closePreparedStatement(ps);
        }
    }

    @Test
    public void closeResultSetTest() {
        boolean result;
        PreparedStatement ps = null;
        ResultSet rs = null;

        Connection con = null;
        try {
            con = dataBaseConfig.getConnection();

            ps = con.prepareStatement(DBConstants.GET_TICKET);
            ps.setString(1,"ABCDEF");

            rs = ps.executeQuery();

            dataBaseConfig.closeResultSet(rs);
            dataBaseConfig.closePreparedStatement(ps);

            result = rs.isClosed();

            assertEquals(true, result);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error closing result set in test.");
        } finally {
            dataBaseConfig.closePreparedStatement(ps);
            dataBaseConfig.closeResultSet(rs);
        }
    }
}
