package com.parkit.parkingsystem.integration;

import com.parkit.parkingsystem.dao.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.TicketDAO;
import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import com.parkit.parkingsystem.integration.service.DataBasePrepareService;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.ParkingService;
import com.parkit.parkingsystem.util.InputReaderUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.concurrent.TimeUnit;

@ExtendWith(MockitoExtension.class)
public class ParkingDataBaseIT {

    private static DataBaseTestConfig dataBaseTestConfig = new DataBaseTestConfig();
    private static ParkingSpotDAO parkingSpotDAO;
    private static TicketDAO ticketDAO;
    private static DataBasePrepareService dataBasePrepareService;
    private static final String REGPLATE = "ABCDEF";

    @Mock
    private static InputReaderUtil inputReaderUtil;

    @BeforeAll
    private static void setUp() throws Exception{
        parkingSpotDAO = new ParkingSpotDAO();
        parkingSpotDAO.dataBaseConfig = dataBaseTestConfig;

        ticketDAO = new TicketDAO();
        ticketDAO.dataBaseConfig = dataBaseTestConfig;

        dataBasePrepareService = new DataBasePrepareService();
    }

    @BeforeEach
    private void setUpPerTest() throws Exception {
        when(inputReaderUtil.readSelection()).thenReturn(1);
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn(REGPLATE);

        dataBasePrepareService.clearDataBaseEntries();
    }

    @AfterAll
    private static void tearDown(){
        dataBasePrepareService.clearDataBaseEntries();
    }

    @Test
    public void testParkingACarSaveATicket(){
        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);

        parkingService.processIncomingVehicle();

        assertEquals(1, parkingService.getAmountOfTickets());
    }
    
    @Test
    public void testParkingACarUpdatesAvailability(){
        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);

        parkingService.processIncomingVehicle();

        assertEquals(1, parkingService.getAmountOfUsedCarSpots());
    }

    @Test
    public void testParkingLotExitSaveCorrectFareAndOutTime(){
        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);

        parkingService.processIncomingVehicle();

        try { // We wait two secs to make sure that we exit not at the same time that we enter. fees are free because we stay less than half an hour.
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) { }
        
        parkingService.processExitingVehicle();

        Ticket ticket = ticketDAO.getTicket(REGPLATE);

        assertEquals(true, ticket.getOutTime().after(ticket.getInTime()));
        assertEquals(true,  ticket.getPrice() == 0);
    }

    @Test
    public void testParkingLotExitUpdatesAvailability(){
        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);

        parkingService.processIncomingVehicle();
        parkingService.processExitingVehicle();

        assertEquals(0, parkingService.getAmountOfUsedCarSpots());
    }

    @Test
    public void testUserHaveADiscountIfHeAlreadyCameBefore() {
        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);

        //Enter and exit the parking for the first time
        parkingService.processIncomingVehicle();
        parkingService.processExitingVehicle();

        //Enter the parking for the second time but don't exit it yet
        parkingService.processIncomingVehicle();

        assertEquals(true, parkingService.willUserHaveADiscount());
    }

    @Test
    public void testUserDontHaveADsicountIfHeNeverCameBefore() {
        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO);

        //Enter the parking for the first time but don't exit it yet
        parkingService.processIncomingVehicle();

        assertEquals(false, parkingService.willUserHaveADiscount());
    }
}