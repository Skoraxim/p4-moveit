package com.parkit.parkingsystem.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

import com.parkit.parkingsystem.constants.DBConstants;

public class DataBaseConfig {

    private static final Logger logger = LogManager.getLogger("DataBaseConfig");

    private String readCreditentials(String propertyToRead) {
        Properties prop = new Properties();
        FileInputStream fis = null;
        String property = null;

        try {
            fis = new FileInputStream("resources/databasecreditentials.properties");
            prop.load(fis);

            property = prop.getProperty(propertyToRead);
        } catch (IOException e) {
            logger.error("Databse creditentials file not found.");
        } finally {
            if(fis != null) {
                try {
                    fis.close();

                    logger.info("Closing File Input Stream");
                } catch (IOException e) {
                    logger.error("Error while closing File Input Stream", e);
                }
            }
        }
        
        return property;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        logger.info("Create DB connection");
        Class.forName(DBConstants.DATABASE_DRIVER);
        return DriverManager.getConnection(DBConstants.DATABASE_URL+DBConstants.DATABASE_NAME_PROD,readCreditentials("username"),readCreditentials("password"));
    }

    public void closeConnection(Connection con){
        if(con!=null){
            try {
                con.close();
                logger.info("Closing DB connection");
            } catch (SQLException e) {
                logger.error("Error while closing connection",e);
            }
        }
    }

    public void closePreparedStatement(PreparedStatement ps) {
        if(ps!=null){
            try {
                ps.close();
                logger.info("Closing Prepared Statement");
            } catch (SQLException e) {
                logger.error("Error while closing prepared statement",e);
            }
        }
    }

    public void closeResultSet(ResultSet rs) {
        if(rs!=null){
            try {
                rs.close();
                logger.info("Closing Result Set");
            } catch (SQLException e) {
                logger.error("Error while closing result set",e);
            }
        }
    }
}
