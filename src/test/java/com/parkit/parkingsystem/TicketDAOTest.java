package com.parkit.parkingsystem;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;
import java.util.Objects;

import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.dao.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.TicketDAO;
import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import com.parkit.parkingsystem.integration.service.DataBasePrepareService;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TicketDAOTest {

    private static DataBaseTestConfig dataBaseTestConfig = new DataBaseTestConfig();
    private static TicketDAO ticketDAO;
    private static DataBasePrepareService dataBasePrepareService;
    private Ticket ticket;
    private ParkingSpotDAO parkingSpotDAO;
    private ParkingSpot parkingSpot;

    @BeforeAll
    private static void setUp() throws Exception {
        ticketDAO = new TicketDAO();
        ticketDAO.dataBaseConfig = dataBaseTestConfig;

        dataBasePrepareService = new DataBasePrepareService();
    }

    @BeforeEach
    private void setUpPerTest() {
        dataBasePrepareService.clearDataBaseEntries();

        parkingSpot = new ParkingSpot(1, ParkingType.CAR, false);
        
        ticket = new Ticket();

        ticket.setId(1);
        ticket.setParkingSpot(parkingSpot);
        ticket.setVehicleRegNumber("ABCDEF");
        ticket.setPrice(10);

        Date inTime = new Date();
        Date outTime = new Date();
        inTime.setTime(System.currentTimeMillis() - ( 3 * 60 * 60 * 1000)); //3 hours parking time
        
        ticket.setInTime(inTime);
        ticket.setOutTime(outTime);
    }

    @AfterAll
    private static void dataBaseCleanUp() {
        dataBasePrepareService.clearDataBaseEntries();
    }


    @Test
    public void getTicketTest() {
        ticketDAO.saveTicket(ticket);

        ticket = null;
        ticket = ticketDAO.getTicket("ABCDEF");

        assertEquals(true, Objects.nonNull(ticket));
    }

    @Test
    public void getTicket_returnNullIfDoesNotExistsTest() {
        assertEquals(null, ticketDAO.getTicket("ABCDEF"));
    }

    @Test
    public void saveTicketTest() {
        Boolean result = null;
        result = ticketDAO.saveTicket(ticket);

        assertEquals(false, result);
    }

    @Test
    public void updateTicketTest() {
        double oldPrice = ticket.getPrice();
        
        ticketDAO.saveTicket(ticket);
        ticket.setPrice(12);
        ticketDAO.updateTicket(ticket);

        ticket = null;
        ticket = ticketDAO.getTicket("ABCDEF");

        boolean isPriceUpdated = ticket.getPrice() != oldPrice ? true : false;

        assertEquals(true, isPriceUpdated);
    }
    
    @Test
    public void updateTicket_ThrowNullPointerExceptionIfDoesNotExistsTest() {
        ticket.setVehicleRegNumber("ABCDEF");

        assertThrows(NullPointerException.class, () -> ticketDAO.updateTicket(ticket));
    }

    @Test
    public void getAmountOfTicketsTest() {
        ticketDAO.saveTicket(ticket);
        ticket.setVehicleRegNumber("GHIJKL");
        ticketDAO.saveTicket(ticket);

        assertEquals(2, ticketDAO.getAmountOfTickets());
    }

    @Test
    public void getAmountOfUsedCarSpotTest() {
        parkingSpotDAO = new ParkingSpotDAO();
        parkingSpotDAO.dataBaseConfig = dataBaseTestConfig;

        ticketDAO.saveTicket(ticket);
        parkingSpotDAO.updateParking(parkingSpot);
        parkingSpot = null;

        ticket.setVehicleRegNumber("GHIJKL");
        parkingSpot = new ParkingSpot(2, ParkingType.CAR, false);
        ticketDAO.saveTicket(ticket);
        parkingSpotDAO.updateParking(parkingSpot);
        parkingSpot = null;

        ticket.setVehicleRegNumber("ZZAABB");
        parkingSpot = new ParkingSpot(3, ParkingType.CAR, false);
        ticketDAO.saveTicket(ticket);
        parkingSpotDAO.updateParking(parkingSpot);

        assertEquals(3, ticketDAO.getAmountOfUsedCarSpots());
    }

}
