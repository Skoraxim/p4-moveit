package com.parkit.parkingsystem.service;

import com.parkit.parkingsystem.constants.Fare;
import com.parkit.parkingsystem.model.Ticket;

public class FareCalculatorService {

    public void calculateFare(Ticket ticket){
        if( (ticket.getOutTime() == null) || (ticket.getOutTime().before(ticket.getInTime())) ){
            throw new IllegalArgumentException("Out time provided is incorrect:"+ticket.getOutTime().toString());
        }
        
        double duration = ticket.getOutTime().getTime() - ticket.getInTime().getTime();
        double durationInHours = duration / 1000 / 60 / 60;

        if(durationInHours <= 0.5){
            ticket.setPrice(0);
        } else {
            switch (ticket.getParkingSpot().getParkingType()){
                case CAR: {
                    ticket.setPrice(durationInHours * Fare.CAR_RATE_PER_HOUR);
                    break;
                }
                case BIKE: {
                    ticket.setPrice(durationInHours * Fare.BIKE_RATE_PER_HOUR);
                    break;
                }
                default: throw new IllegalArgumentException("Unknown Parking Type");
            }
            
        }
    }

    public void calculateFare(Ticket ticket, boolean applyDiscount){
        if(applyDiscount){
            calculateFare(ticket);
            ticket.setPrice(ticket.getPrice()*0.95);
        } else {
            calculateFare(ticket);
        }
    }
}